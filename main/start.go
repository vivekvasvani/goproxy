package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/creack/goproxy"
	"github.com/creack/goproxy/registry"
)

var ServiceRegistry = registry.DefaultRegistry{
	"service1": {
		"EK": {
			"mercury-uat.phonepe.com:80",
			"192.168.12.10:8081",
		},
	},
	"service2": {
		"DE": {
			"d7lms.myntra.com:80",
			"192.168.12.10:8081",
		},
	},
	"service3": {
		"WALLET": {
			"192.168.13.228:6070",
			"192.168.12.10:8081",
		},
	},
}

func main() {
	http.HandleFunc("/", goproxy.NewMultipleHostReverseProxy(ServiceRegistry))
	http.HandleFunc("/health", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, "%v\n", ServiceRegistry)
	})
	println("ready")
	log.Fatal(http.ListenAndServe(":9090", nil))
}